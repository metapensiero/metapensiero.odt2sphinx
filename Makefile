# -*- coding: utf-8 -*-
# :Project:   metapensiero.odt2sphinx -- ODT to RST converter
# :Created:   gio 05 nov 2015 12:04:38 CET
# :Author:    Lele Gaifax <lele@metapensiero.it>
# :License:   Python Software Foundation License
# :Copyright: Copyright (C) 2015 Lele Gaifax
#

all: help

help::
	@printf "check\n\tRun tox test suite\n"

check:
	@tox

include Makefile.release

