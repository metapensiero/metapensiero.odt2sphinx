==========
 Bestiary
==========

-------------------------------
 A collection of strange cases
-------------------------------

There is an empty bold span after here: it must be skipped

Here instead the bold spans include a space respectively at the end and at the start:

**BAD** SPAN and BAD **SPAN**

Some text, followed by a space, then a line-break

This paragraph *ends* with a line-break

This *starts* with a line-break

This is a strange list: the four items belong to a nested list, so they are at the
*second* level, but the first level isn't visible… I copied&pasted it, because I could not
even understand how it was written to start with!

a) * a

   * b

   * c

   * d

The following word is composed by two spans of bold characters, but with different sizes:
**StrangeText**

The following heading is composed by three anchors (even the space!):

`FOO BAR`__
-----------

__ http://foo/bar
